'use strict'

function filterBy(arr, type) {
    return arr.filter(word => typeof word !== type);
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));